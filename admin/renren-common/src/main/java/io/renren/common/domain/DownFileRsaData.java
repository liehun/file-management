package io.renren.common.domain;/*
 * @Author WuYu
 * @Description
 * @Date 2023/09/08 10/05
 **/

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DownFileRsaData {

    @ApiModelProperty("文件id")
    private Integer id;

    @ApiModelProperty("用户id")
    private Long userId;

    @ApiModelProperty("时间")
    private Date time;

}
