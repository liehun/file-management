package io.renren.common.exception;/*
 * @Author wuyu
 * @Description
 * @Date 2022/5/16 0016 14:05
 **/

public class BusinessException extends RuntimeException{

    private static final long serialVersionUID = 1L;

    private int code = 0;

    //消息
    private String msg;

    public BusinessException(int code, String message) {
        super(message);
        this.code = code;
        this.msg = message;
    }

    public BusinessException(String message) {
        super(message);
        this.code = 202;
        this.msg = message;
    }

    public String getMsg(){
        return this.msg;
    }

    public int getCode() {
        return this.code;
    }

}
