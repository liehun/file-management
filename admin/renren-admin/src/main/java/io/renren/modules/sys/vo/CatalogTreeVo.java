package io.renren.modules.sys.vo;/*
 * @Author WuYu
 * @Description
 * @Date 2023/09/07 21/36
 **/

import lombok.Data;

import java.util.List;

@Data
public class CatalogTreeVo {

    private Integer id;

    private Integer parentId;

    private String parentName;

    private String name;

    private List<CatalogTreeVo> children;

}
