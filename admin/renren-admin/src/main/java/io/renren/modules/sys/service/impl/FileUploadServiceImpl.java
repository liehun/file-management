package io.renren.modules.sys.service.impl;/*
 * @Author WuYu
 * @Description
 * @Date 2023/09/07 15/06
 **/

import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.net.URLDecoder;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Element;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import io.renren.common.domain.DownFileRsaData;
import io.renren.common.exception.BusinessException;
import io.renren.common.redis.RedisUtils;
import io.renren.common.utils.RsaUtils;
import io.renren.modules.security.user.SecurityUser;
import io.renren.modules.security.user.UserDetail;
import io.renren.modules.sys.dao.SysDeptDao;
import io.renren.modules.sys.dao.file.CatalogMapper;
import io.renren.modules.sys.dao.file.FileUploadMapper;
import io.renren.modules.sys.dto.SearchFileDto;
import io.renren.modules.sys.dto.SysUserDTO;
import io.renren.modules.sys.entity.SysDeptEntity;
import io.renren.modules.sys.entity.file.AuthCatalog;
import io.renren.modules.sys.entity.file.Catalog;
import io.renren.modules.sys.entity.file.FileUpload;
import io.renren.modules.sys.service.AuthCatalogService;
import io.renren.modules.sys.service.FileUploadService;
import io.renren.modules.sys.service.SysDeptService;
import io.renren.modules.sys.service.SysUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.sql.rowset.serial.SerialException;
import java.io.*;
import java.math.BigInteger;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Date;
import java.util.List;

@Slf4j
@Service
public class FileUploadServiceImpl extends ServiceImpl<FileUploadMapper, FileUpload> implements FileUploadService {

    @Value("${file.path}")
    private String fileBasePath;

    @Value("${pdf.fontPath}")
    private String chineseFront;

    @Value("${pdf.temp}")
    private String pdfTempPath;

    @Resource
    private RedisUtils redisUtils;

    @Resource
    private SysDeptDao deptDao;

    @Resource
    private RsaUtils rsaUtils;

    @Resource
    private AuthCatalogService authCatalogService;

    @Resource
    private SysUserService userService;

    @Resource
    private CatalogMapper catalogMapper;

    @Override
    public void upload(Integer catalogId, MultipartFile file) {

        List<Catalog> allParent = catalogMapper.getAllParent(catalogId);
        StringBuilder newFilePath = new StringBuilder(fileBasePath);
        for (Catalog item : allParent) {
            newFilePath.append("/").append(item.getName());
        }

        boolean exist = FileUtil.exist(newFilePath.toString());
        if (!exist) {
            FileUtil.createTempFile(new File(newFilePath.toString()));
        }

        String originName = file.getOriginalFilename();

        if (originName == null) {
            throw new BusinessException("文件获取失败");
        }

        int suffixIndex = originName.lastIndexOf(".");
        if (suffixIndex < 0) {
            throw new BusinessException("文件名称不正确");
        }

        String suffix = originName.substring(suffixIndex+1);


        String newFileName = DateUtil.format(new Date(), "yyyyMMddHHmmssSSS") + RandomUtil.randomNumbers(3) +"."+suffix;

        File tempFile = null;
        try {
            tempFile = createFile(file.getInputStream(), newFilePath.toString(), newFileName);

            FileUpload fileUpload = new FileUpload();
            fileUpload.setFileName(newFileName);
            fileUpload.setOriginName(originName);
            fileUpload.setSuffix(suffix);
            fileUpload.setCatalogId(catalogId);
            fileUpload.setFilePath(newFilePath.toString());

            this.save(fileUpload);

        } catch (Exception e) {
            if (tempFile != null) {
                FileUtil.del(tempFile);
            }
            throw new BusinessException("保存文件失败");
        }

    }

    @Override
    public void downFile(String key , HttpServletResponse response) {

        try {

            Object o = redisUtils.get(key);
            if (o == null) {
                System.out.println("过期");
                return;
            }

            String decode = o.toString();

            System.out.println("数据："+decode);

            DownFileRsaData bean = JSONUtil.toBean(JSONUtil.parseObj(decode), DownFileRsaData.class);

            SysUserDTO userInfo = userService.get(bean.getUserId());

            boolean haveAuth = false;


            Integer id = bean.getId();
            FileUpload fileUpload = this.getById(id);

            if (Integer.valueOf("1").equals(userInfo.getSuperAdmin())) {
                haveAuth = true;
            } else {
                AuthCatalog authCatalog = authCatalogService.getBy(fileUpload.getCatalogId(), BigInteger.valueOf(bean.getUserId()));
                haveAuth = authCatalog != null;
            }

            if (haveAuth) {
                if (fileUpload != null) {
                    String filePath = fileUpload.getFilePath() + "/" + fileUpload.getFileName();
                    System.out.println("文件路径：" + filePath);
                    if (FileUtil.exist(filePath)) {

                        String fileName = URLEncoder.encode(fileUpload.getOriginName(), "UTF-8").replace("+", "%20");

                        response.setContentType("application/octet-stream");
                        response.setHeader("Content-Disposition", "attachment; filename=" + fileName);

                        File file = new File(filePath);
                        boolean del = false;
                        //如果是pdf的话，需要生成水印，当前用户的名字和工号还有部门
                        if (fileName.indexOf("pdf") > 0) {

                            String msg = userInfo.getRealName() + "(" + userInfo.getNum() + ")";
                            SysDeptEntity dao = deptDao.getById(userInfo.getDeptId());
                            if (dao != null) {
                                msg = dao.getName() + ":" + msg;
                            }


                            file = new File(createWater(filePath, msg));
                            del = true;
                        }
                        FileInputStream inputStream = new FileInputStream(file);
                        ServletOutputStream outputStream = response.getOutputStream();

                        // 将文件内容写入响应输出流
                        byte[] buffer = new byte[1024];
                        int bytesRead;
                        while ((bytesRead = inputStream.read(buffer)) != -1) {
                            outputStream.write(buffer, 0, bytesRead);
                        }

                        // 关闭输入流和输出流
                        inputStream.close();
                        outputStream.close();

                        if (del) {
                            FileUtil.del(file);
                        }
                    } else {
                        System.out.println("文件不存在");
                    }
                }

            }
        } catch (Exception e) {
            System.out.println("c");
        }

    }

    private String createWater(String file, String msg) {
        try {

            String fileName = RandomUtil.randomNumbers(32) + ".pdf";

            String filePath = pdfTempPath + fileName;

            PdfReader reader = new PdfReader(file);
            PdfStamper stamper = new PdfStamper(reader, Files.newOutputStream(Paths.get(filePath)));

            int pageCount = reader.getNumberOfPages();
            for (int i = 1; i <= pageCount; i++) {
                PdfContentByte content = stamper.getOverContent(i);
                // 在每个页面上添加水印
                BaseFont baseFont = BaseFont.createFont(chineseFront, BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
                content.beginText();
                content.setFontAndSize(baseFont, 30);
                content.setColorFill(BaseColor.GRAY);
                content.showTextAligned(Element.ALIGN_CENTER, msg, 300, 400, 45);
                content.endText();
            }

            stamper.close();
            reader.close();

            return filePath;

        } catch (Exception e) {
            throw new RuntimeException("pdf文件水印生成失败");
        }
    }

    @Override
    public boolean haveFile(Integer catalogId) {

        LambdaQueryWrapper<FileUpload> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(FileUpload::getCatalogId, catalogId);

        return this.count(wrapper) != 0;
    }

    @Override
    public IPage<FileUpload> getList(SearchFileDto params, Long userId) {

        IPage<FileUpload> page = new Page<>(params.getPageIndex(), params.getPageSize());

        baseMapper.getList(page, params, userId);

        UserDetail user = SecurityUser.getUser();

        for (FileUpload item : page.getRecords()) {
            item.setDel(user.getSuperAdmin().equals(Integer.valueOf("1")) || item.getCreator().equals(user.getId()));
        }

        return page;
    }


    private File createFile(InputStream fileInputStream, String path, String fileName) {

        try {
            String filePath = path + "/" + fileName;

            FileOutputStream fos = new FileOutputStream(filePath);

            int index = 0;

            byte[] bytes = new byte[1024];

            while ((index = fileInputStream.read(bytes)) != -1) {
                fos.write(bytes, 0, index);
            }

            fos.close();
            fileInputStream.close();

            return new File(filePath);

        } catch (Exception e) {
            throw new BusinessException("保存文件失败");
        }

    }
}
