package io.renren.modules.sys.dto;/*
 * @Author WuYu
 * @Description
 * @Date 2023/09/08 00/47
 **/

import lombok.Data;

@Data
public class SearchDto {

    private Integer pageIndex = 1;

    private Integer pageSize = 20;
}
