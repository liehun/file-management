package io.renren.modules.sys.dto;/*
 * @Author WuYu
 * @Description
 * @Date 2023/09/07 15/29
 **/

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

@Data
public class AuthCatalogDto {

    @ApiModelProperty("授权目录")
    @NotNull(message = "请选择目录")
    private List<Integer> list = new ArrayList<>();

    @ApiModelProperty("授权给谁")
    @NotNull(message = "请选择用户")
    private BigInteger toUid;

}
