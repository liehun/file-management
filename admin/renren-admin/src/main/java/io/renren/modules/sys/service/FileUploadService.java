package io.renren.modules.sys.service;/*
 * @Author WuYu
 * @Description
 * @Date 2023/09/07 15/06
 **/

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.modules.sys.dto.SearchFileDto;
import io.renren.modules.sys.entity.file.FileUpload;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;

public interface FileUploadService extends IService<FileUpload> {

    void upload(Integer catalogId, MultipartFile file);

    void downFile(String id, HttpServletResponse response);

    boolean haveFile(Integer catalogId);

    IPage<FileUpload> getList(SearchFileDto params, Long userId);
}
