package io.renren.modules.sys.dao.file;/*
 * @Author WuYu
 * @Description
 * @Date 2023/09/07 15/03
 **/

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.renren.modules.sys.dto.SearchFileDto;
import io.renren.modules.sys.entity.file.FileUpload;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface FileUploadMapper extends BaseMapper<FileUpload> {

    IPage<FileUpload> getList(IPage<FileUpload> page, @Param("params") SearchFileDto params, @Param("userId") Long userId);
}
