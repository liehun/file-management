package io.renren.modules.sys.dao.file;/*
 * @Author WuYu
 * @Description
 * @Date 2023/09/07 15/01
 **/

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.renren.modules.sys.dto.SearchAuthUserDto;
import io.renren.modules.sys.entity.SysUserEntity;
import io.renren.modules.sys.entity.file.AuthCatalog;
import io.renren.modules.sys.entity.file.Catalog;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface AuthCatalogMapper extends BaseMapper<AuthCatalog> {

    IPage<SysUserEntity> getUserByCatalogId(IPage<SysUserEntity> page, @Param("params") SearchAuthUserDto params);

    List<Catalog> getAllChildren(@Param("id") Integer id);

}
