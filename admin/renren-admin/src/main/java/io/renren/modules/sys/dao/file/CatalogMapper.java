package io.renren.modules.sys.dao.file;/*
 * @Author WuYu
 * @Description
 * @Date 2023/09/07 11/23
 **/

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.renren.modules.sys.entity.file.Catalog;
import io.renren.modules.sys.vo.CatalogTreeVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.math.BigInteger;
import java.util.List;

@Mapper
public interface CatalogMapper extends BaseMapper<Catalog> {

    List<Catalog> getAllParent(@Param("id") Integer id);

    List<CatalogTreeVo> getTree(@Param("userId") Long userId, @Param("name") String name);
}
