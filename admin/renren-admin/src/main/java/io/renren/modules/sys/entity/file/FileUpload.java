package io.renren.modules.sys.entity.file;/*
 * @Author WuYu
 * @Description
 * @Date 2023/09/07 14/58
 **/

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@TableName("file_upload")
@Data
public class FileUpload extends BaseCommEntity {

    @ApiModelProperty("目录id")
    private Integer catalogId;

    @ApiModelProperty("原始文件名称")
    private String originName;

    @ApiModelProperty("上传后的文件名称")
    private String fileName;

    @ApiModelProperty("文件路径")
    private String filePath;

    @ApiModelProperty("文件类型")
    private String suffix;

    @TableField(exist = false)
    private String uploadName;

    @TableField(exist = false)
    private String realName;

    @TableField(exist = false)
    private String userName;

    @TableField(exist = false)
    public boolean del;
}
