package io.renren.modules.sys.service.impl;/*
 * @Author WuYu
 * @Description
 * @Date 2023/09/07 11/24
 **/

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.io.FileUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.utils.CommonUtils;
import io.renren.modules.security.user.SecurityUser;
import io.renren.modules.security.user.UserDetail;
import io.renren.modules.sys.dao.file.CatalogMapper;
import io.renren.modules.sys.dto.AddCatalogDto;
import io.renren.modules.sys.entity.file.Catalog;
import io.renren.modules.sys.service.CatalogService;
import io.renren.modules.sys.vo.CatalogTreeVo;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CatalogServiceImpl extends ServiceImpl<CatalogMapper, Catalog> implements CatalogService {

    @Value("${file.path}")
    private String fileBasePath;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void add(AddCatalogDto params) {

        Integer id = CommonUtils.getId(params.getId());

        Catalog byId = this.getById(id);

        Catalog exist = this.getBy(params.getName(), params.getParentId());
        if (exist != null && !id.equals(exist.getId())) {
            throw new RuntimeException("目录名称已存在");
        }

        if (byId == null) {
            byId = new Catalog();
        }

        BeanUtil.copyProperties(params, byId);
        this.saveOrUpdate(byId);

        createDir(byId);
    }

    private void createDir(Catalog catalog) {

        Integer parentId = catalog.getParentId();
        StringBuilder newFilePath = new StringBuilder(fileBasePath);

        //递归寻找上级
        if (parentId == 0) {
            newFilePath.append("/").append(catalog.getName());
        } else {
            List<Catalog> allParent = baseMapper.getAllParent(catalog.getId());
            for (Catalog item : allParent) {
                newFilePath.append("/").append(item.getName());
            }
        }

        //判断目录是否存在，没有存在创建
        boolean exist = FileUtil.exist(newFilePath.toString());
        if (!exist) {
            FileUtil.createTempFile(new File(newFilePath.toString()));
        }

    }

    @Override
    public Catalog getBy(String name, Integer parentId) {

        LambdaQueryWrapper<Catalog> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Catalog::getName, name);
        wrapper.eq(Catalog::getParentId, parentId);
        return this.getOne(wrapper);
    }

    @Override
    public void remove(Integer id) {

        //todo 判断下改目录下面是否已经存在文件了

    }

    @Override
    public List<CatalogTreeVo> getTree() {

        List<CatalogTreeVo> tree = getAllList("");

        List<CatalogTreeVo> first = tree.stream().filter(s -> Integer.valueOf("0").equals(s.getParentId())).collect(Collectors.toList());

        deal(tree, first);

        return first;
    }

    @Override
    public List<CatalogTreeVo> getAllList(String name) {
        UserDetail user = SecurityUser.getUser();

        Long userId = Integer.valueOf("1").equals(user.getSuperAdmin()) ? null : user.getId();

        return  baseMapper.getTree(userId, name);
    }

    @Override
    public boolean isLast(String id) {

        LambdaQueryWrapper<Catalog> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Catalog::getParentId, id);

        return this.count(wrapper) == 0;
    }

    private void deal(List<CatalogTreeVo> list, List<CatalogTreeVo> childrenList) {
        for (CatalogTreeVo item : childrenList) {
            List<CatalogTreeVo> children = getChildren(list, item.getId());
            item.setChildren(children);
            if (!children.isEmpty()) {
                deal(list, children);
            }
        }
    }

    private List<CatalogTreeVo> getChildren(List<CatalogTreeVo> allList, Integer id) {
        List<CatalogTreeVo> collect = allList.stream().filter(s -> s.getParentId().equals(id)).collect(Collectors.toList());
        allList.removeAll(collect);
        return collect;
    }
}
