package io.renren.modules.sys.dto;/*
 * @Author WuYu
 * @Description
 * @Date 2023/09/07 11/26
 **/

import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class AddCatalogDto {

    private Integer id;

    @NotEmpty(message = "请填写目录名称")
    private String name;

    private Integer parentId = 0;

}
