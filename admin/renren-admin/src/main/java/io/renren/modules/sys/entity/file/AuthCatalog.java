package io.renren.modules.sys.entity.file;/*
 * @Author WuYu
 * @Description
 * @Date 2023/09/07 14/55
 **/

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigInteger;

@EqualsAndHashCode(callSuper = true)
@Data
@TableName("file_auth_catalog")
public class AuthCatalog extends BaseCommEntity{

    @ApiModelProperty("目录id")
    private Integer catalogId;

    @ApiModelProperty("用户id")
    private BigInteger userId;
}
