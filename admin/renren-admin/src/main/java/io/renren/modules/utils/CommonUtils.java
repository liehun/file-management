package io.renren.modules.utils;/*
 * @Author WuYu
 * @Description
 * @Date 2023/10/22 09/49
 **/

import cn.hutool.poi.excel.ExcelReader;
import cn.hutool.poi.excel.ExcelUtil;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class CommonUtils {

    public static List<Map<String, Object>> getDataFromExcelFile(MultipartFile file) {

        List<Map<String, Object>> maps = new ArrayList<>();

        try {

            ExcelReader reader = ExcelUtil.getReader(file.getInputStream());

            maps = reader.readAll();

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return maps;

    }
}
