package io.renren.modules.sys.service.impl;/*
 * @Author WuYu
 * @Description
 * @Date 2023/09/12 16/54
 **/

import cn.hutool.http.HttpRequest;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import io.renren.common.exception.BusinessException;
import io.renren.common.redis.RedisUtils;
import io.renren.modules.sys.dto.TimeData;
import io.renren.modules.sys.dto.WxInfo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Service
public class WxServiceImpl {


    private final String corpid = "wxc8f1e3961829f654";

    private final String corpsecret = "UoAhWMyXMTSt_Ez94x31-Y_804r7w9tcOfW01aIZDms";

    private TimeData ACCESS_TOKEN = null;

    public WxInfo getInfo(String code) {

        String accessToken = getAccessToken();

        String url = "https://qyapi.weixin.qq.com/cgi-bin/auth/getuserinfo?access_token=" + accessToken + "&code=" + code;

        String body = http("get", url, null);

        JSONObject jsonObject = JSONUtil.parseObj(body);

        String userTicket = jsonObject.getStr("user_ticket");

        String nextUrl = "https://qyapi.weixin.qq.com/cgi-bin/auth/getuserdetail?access_token=" + accessToken;

        Map<String, Object> map = new HashMap<>();
        map.put("user_ticket", userTicket);

        String postBody = http("post", nextUrl, map);

        return JSONUtil.toBean(JSONUtil.parseObj(postBody), WxInfo.class);
    }

    public String getAccessToken() {

        if (ACCESS_TOKEN != null) {
            if (ACCESS_TOKEN.getDate() != null && ACCESS_TOKEN.getDate().compareTo(new Date()) <= 5000) {
                return ACCESS_TOKEN.getValue();
            }
        }


        String url = "https://qyapi.weixin.qq.com/cgi-bin/gettoken?corpid=" + corpid + "&corpsecret=" + corpsecret;

        String body = http("get", url, null);

        JSONObject jsonObject = JSONUtil.parseObj(body);

        String accessToken = jsonObject.getStr("access_token");

        ACCESS_TOKEN = new TimeData(accessToken, new Date());

        return accessToken;
    }

    public String http(String method, String url, Map<String, Object> map) {

        HttpRequest request;

        if ("post".equalsIgnoreCase(method)) {
            request = HttpRequest.post(url)
                    .header("Content-Type", "application/json;charset=utf-8 ")
                    .body(JSONUtil.toJsonStr(map));
        } else {
            request = HttpRequest.get(url)
                    .header("Content-Type", "application/x-www-form-urlencoded;charset=utf-8");
        }

        String body = request.timeout(3000).execute().body();

        JSONObject jsonObject = JSONUtil.parseObj(body);
        if (!jsonObject.getStr("errcode", "0").equals("0")) {
            throw new BusinessException(jsonObject.getStr("errmsg"));
        }

        return body;
    }

}
