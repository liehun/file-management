package io.renren.modules.sys.dto;/*
 * @Author WuYu
 * @Description
 * @Date 2023/09/12 17/05
 **/

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class WxInfo {
    private String userid;
    private String gender;
    private String avatar;
    private String qrCode;
    private String mobile;
    private String email;
    private String bizMail;
    private String address;
}
