package io.renren.modules.sys.service;/*
 * @Author WuYu
 * @Description
 * @Date 2023/09/07 11/24
 **/

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.modules.sys.dto.AddCatalogDto;
import io.renren.modules.sys.entity.file.Catalog;
import io.renren.modules.sys.vo.CatalogTreeVo;

import java.util.List;

public interface CatalogService extends IService<Catalog> {

    void add(AddCatalogDto params);

    Catalog getBy(String name, Integer parentId);

    void remove(Integer id);

    List<CatalogTreeVo> getTree();

    List<CatalogTreeVo> getAllList(String name);

    boolean isLast(String id);
}
