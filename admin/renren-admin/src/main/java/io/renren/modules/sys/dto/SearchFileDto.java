package io.renren.modules.sys.dto;/*
 * @Author WuYu
 * @Description
 * @Date 2023/09/08 00/48
 **/

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class SearchFileDto extends SearchDto{

    @ApiModelProperty("目录id")
    private Integer catalogId;

    @ApiModelProperty("文件名称")
    private String name;

}
