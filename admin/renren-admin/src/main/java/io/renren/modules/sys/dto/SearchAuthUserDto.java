package io.renren.modules.sys.dto;/*
 * @Author WuYu
 * @Description
 * @Date 2023/09/09 08/43
 **/

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class SearchAuthUserDto extends SearchDto {

    @ApiModelProperty("授权用户姓名")
    private String name;

    @ApiModelProperty("目录id")
    private Integer catalogId;

}
