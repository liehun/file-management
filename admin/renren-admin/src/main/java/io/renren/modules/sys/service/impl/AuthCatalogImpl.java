package io.renren.modules.sys.service.impl;/*
 * @Author WuYu
 * @Description
 * @Date 2023/09/07 15/07
 **/

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.exception.BusinessException;
import io.renren.modules.sys.dao.file.AuthCatalogMapper;
import io.renren.modules.sys.dao.file.CatalogMapper;
import io.renren.modules.sys.dto.AuthCatalogDto;
import io.renren.modules.sys.dto.CancelAuthDto;
import io.renren.modules.sys.dto.SearchAuthUserDto;
import io.renren.modules.sys.entity.SysUserEntity;
import io.renren.modules.sys.entity.file.AuthCatalog;
import io.renren.modules.sys.entity.file.Catalog;
import io.renren.modules.sys.service.AuthCatalogService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class AuthCatalogImpl extends ServiceImpl<AuthCatalogMapper, AuthCatalog> implements AuthCatalogService {

    @Resource
    private CatalogMapper catalogMapper;

    @Override
    public void auth(AuthCatalogDto params) {

        if (params.getList().isEmpty()) {
            throw new BusinessException("请选择目录");
        }

        List<Integer> list = new ArrayList<>();

        for (Integer tem : params.getList()) {
            List<Catalog> allParent = catalogMapper.getAllParent(tem);
            list.addAll(allParent.stream().map(Catalog::getId).collect(Collectors.toList()));
        }

        list = list.stream().distinct().collect(Collectors.toList());

        for (Integer catalogId : list) {
            AuthCatalog by = this.getBy(catalogId, params.getToUid());
            if (by == null) {
                by = new AuthCatalog();
                by.setCatalogId(catalogId);
                by.setUserId(params.getToUid());
                this.save(by);
            }
        }

    }

    @Override
    public void cancel(CancelAuthDto params) {

        List<Catalog> list = baseMapper.getAllChildren(params.getCatalogId());
        for (Catalog item : list) {
            LambdaQueryWrapper<AuthCatalog> wrapper = new LambdaQueryWrapper<>();
            wrapper.eq(AuthCatalog::getCatalogId, item.getId());
            wrapper.eq(AuthCatalog::getUserId, params.getUserId());
            this.remove(wrapper);
        }
    }

    @Override
    public AuthCatalog getBy(Integer catalogId, BigInteger userId) {

        LambdaQueryWrapper<AuthCatalog> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(AuthCatalog::getCatalogId, catalogId);
        wrapper.eq(AuthCatalog::getUserId, userId);

        return this.getOne(wrapper);
    }

    @Override
    public IPage<SysUserEntity> getUser(SearchAuthUserDto params) {

        IPage<SysUserEntity> page = new Page<>(params.getPageIndex(), params.getPageSize());


        return baseMapper.getUserByCatalogId(page, params);
    }
}
