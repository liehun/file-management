package io.renren.modules.sys.service;/*
 * @Author WuYu
 * @Description
 * @Date 2023/09/07 15/06
 **/

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.modules.sys.dto.AuthCatalogDto;
import io.renren.modules.sys.dto.CancelAuthDto;
import io.renren.modules.sys.dto.SearchAuthUserDto;
import io.renren.modules.sys.entity.SysUserEntity;
import io.renren.modules.sys.entity.file.AuthCatalog;

import java.math.BigInteger;
import java.util.List;

public interface AuthCatalogService extends IService<AuthCatalog> {

    void auth(AuthCatalogDto params);

    void cancel(CancelAuthDto params);

    AuthCatalog getBy(Integer catalogId, BigInteger userId);

    IPage<SysUserEntity> getUser(SearchAuthUserDto params);
}
