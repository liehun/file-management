package io.renren.modules.sys.controller;/*
 * @Author WuYu
 * @Description
 * @Date 2023/09/07 11/44
 **/

import cn.hutool.core.date.DateUtil;
import cn.hutool.crypto.SecureUtil;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import io.renren.common.domain.DownFileRsaData;
import io.renren.common.exception.BusinessException;
import io.renren.common.redis.RedisUtils;
import io.renren.common.utils.CommonUtils;
import io.renren.common.utils.Result;
import io.renren.common.utils.RsaUtils;
import io.renren.modules.security.user.SecurityUser;
import io.renren.modules.security.user.UserDetail;
import io.renren.modules.sys.dao.SysUserDao;
import io.renren.modules.sys.dto.*;
import io.renren.modules.sys.entity.SysUserEntity;
import io.renren.modules.sys.entity.file.AuthCatalog;
import io.renren.modules.sys.entity.file.FileUpload;
import io.renren.modules.sys.service.AuthCatalogService;
import io.renren.modules.sys.service.CatalogService;
import io.renren.modules.sys.service.FileUploadService;
import io.renren.modules.sys.service.SysUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.SneakyThrows;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.math.BigInteger;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.*;

@RestController
@RequestMapping("/file")
@Api(tags = "文件管理")
public class FileController {

    @Resource
    private CatalogService catalogService;

    @Resource
    private RedisUtils redisUtils;

    @Resource
    private AuthCatalogService authCatalogService;

    @Resource
    private FileUploadService uploadService;

    @Resource
    private RsaUtils rsaUtils;

    @Resource
    private CommonUtils commonUtils;

    @Resource
    private SysUserDao userDao;

    @GetMapping("tree")
    @ApiOperation("获取树形")
    public Result<Object> tree() {
        return new Result<>().ok(catalogService.getTree());
    }

    @GetMapping("getAllCatalog")
    @ApiOperation("获取所有目录")
    public Result<Object> getAllCatalog(@RequestParam(defaultValue = "") String name) {
        return new Result<>().ok(catalogService.getAllList(name));
    }

    @GetMapping("user")
    @ApiOperation("用户")
    public Result<Object> getUserList(){
        LambdaQueryWrapper<SysUserEntity> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(SysUserEntity::getSuperAdmin, 0);

        return new Result<>().ok(userDao.selectList(wrapper));
    }

    @PostMapping("catalogUser")
    @ApiOperation("目录授权人集合")
    public Result<Object> catalogUser(@RequestBody SearchAuthUserDto params) {
        return new Result<>().ok(authCatalogService.getUser(params));
    }


    @PostMapping("add")
    @ApiOperation("添加目录")
    public Result<Object> add(@RequestBody @Validated AddCatalogDto params) {
        catalogService.add(params);
        return new Result<>().ok("操作成功");
    }

    @GetMapping("del")
    @ApiOperation("删除目录")
    @Transactional(rollbackFor = Exception.class)
    public Result<Object> del(@RequestParam String ids) {

        List<String> list = Arrays.asList(ids.split(","));
        if (list.isEmpty()) {
            throw new BusinessException("未选取数据");
        }

        for (String key : list) {
            //是否是最低级
            if (!catalogService.isLast(key)) {
                throw new BusinessException("请选择最低级");
            }

            //需要判断次目录下是否有文件
            if (uploadService.haveFile(Integer.valueOf(key))) {
                throw new BusinessException("改目录下存在文件");
            }

            catalogService.removeById(Integer.valueOf(key));
        }

        return new Result<>().ok("操作成功");
    }

    @PostMapping("authCatalog")
    @ApiOperation("授权目录")
    public Result<Object> authCatalog(@RequestBody @Validated AuthCatalogDto params) {
        authCatalogService.auth(params);
        return new Result<>().ok("操作成功");
    }

    @PostMapping("cancelAuthDto")
    @ApiOperation("取消授权")
    public Result<Object> cancelAuthDto(@RequestBody @Validated CancelAuthDto params) {
        authCatalogService.cancel(params);
        return new Result<>().ok("操作成功");
    }

    @PostMapping("uploadFile")
    @ApiOperation("上传文件")
    public Result<Object> uploadFile(@RequestParam Integer catalogId, @RequestPart("file") MultipartFile file) {
        UserDetail user = SecurityUser.getUser();
        if (!Integer.valueOf("1").equals(user.getSuperAdmin())) {
            AuthCatalog exist = authCatalogService.getBy(catalogId, BigInteger.valueOf(user.getId()));
            if (exist == null) {
                throw new BusinessException("你未拥有改目录的权限");
            }
        }

        uploadService.upload(catalogId, file);


        return new Result<>().ok("上传成功");
    }

    @PostMapping("fileList")
    @ApiOperation("文件")
    public Result<Object> fileList(@RequestBody SearchFileDto params) {

        UserDetail user = SecurityUser.getUser();

        Long userId = Integer.valueOf("1").equals(user.getSuperAdmin()) ? null : user.getId();

        return new Result<>().ok(uploadService.getList(params, userId));
    }

    @GetMapping("delFile")
    @ApiOperation("删除文件")
    public Result<Object> delFile(@RequestParam Integer id){
        uploadService.removeById(id);
        return new Result<>().ok("删除成功");
    }

    @GetMapping("downFile")
    @ApiOperation("下载文件")
    public void uploadFile(@RequestParam String key, HttpServletResponse response) {
        uploadService.downFile(key, response);
    }

    @GetMapping("getDownKey")
    @ApiOperation("获取下载key")
    @SneakyThrows
    public Result<Object> getDownKey(@RequestParam Integer id) {


        UserDetail user = SecurityUser.getUser();

        DownFileRsaData downFileRsaData = new DownFileRsaData(id, user.getId(), new Date());

/*        String encrypt = rsaUtils.encrypt(JSONUtil.toJsonStr(downFileRsaData));

        String key = URLEncoder.encode(encrypt, "UTF-8");*/

        String key = SecureUtil.md5(JSONUtil.toJsonStr(downFileRsaData));
        redisUtils.set(key, JSONUtil.toJsonStr(downFileRsaData), 60);

        return new Result<>().ok(commonUtils.uploadUrl + "?key=" + key);

    }
}
