package io.renren.modules.sys.entity.file;/*
 * @Author WuYu
 * @Description
 * @Date 2023/09/07 11/19
 **/

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

@EqualsAndHashCode(callSuper = true)
@TableName("file_catalog")
@Data
public class Catalog extends BaseCommEntity{

    @ApiModelProperty("父级id")
    private Integer parentId;

    @ApiModelProperty("文件名称")
    private String name;

}
