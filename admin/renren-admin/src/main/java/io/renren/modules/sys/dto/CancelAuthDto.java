package io.renren.modules.sys.dto;/*
 * @Author WuYu
 * @Description
 * @Date 2023/09/07 23/51
 **/

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class CancelAuthDto {

    @ApiModelProperty("目录id")
    @NotNull(message = "目录不能为空")
    private Integer catalogId;

    @ApiModelProperty("用户")
    @NotNull(message = "用户不能为空")
    private Long userId;
}
