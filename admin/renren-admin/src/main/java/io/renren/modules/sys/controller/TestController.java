package io.renren.modules.sys.controller;/*
 * @Author WuYu
 * @Description
 * @Date 2023/09/08 09/34
 **/

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.URLUtil;
import cn.hutool.json.JSONUtil;
import io.renren.common.utils.Result;
import io.renren.common.utils.RsaUtils;
import io.renren.modules.sys.entity.file.FileUpload;
import io.renren.modules.sys.service.FileUploadService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.io.File;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("test")
public class TestController {

    @Resource
    private RsaUtils rsaUtils;

    @Resource
    private FileUploadService fileUploadService;

    @GetMapping("index")
    public Result<Object> index( ) {

        List<FileUpload> list = fileUploadService.list();

        for (FileUpload item : list) {
            String filePath = item.getFilePath() + "/" + item.getFileName();
            File file = new File(filePath);
            if (!FileUtil.exist(file)) {
                fileUploadService.removeById(item.getId());
            }
        }

        return new Result<>().ok("123");
    }

}
