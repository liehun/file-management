package io.renren.common.utils;/*
 * @Author WuYu
 * @Description
 * @Date 2023/09/08 09/09
 **/


import cn.hutool.crypto.asymmetric.KeyType;
import cn.hutool.crypto.asymmetric.RSA;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

@Component
public class RsaUtils {

    @Value("${rsa.publicKey}")
    private String publicKey;

    @Value("${rsa.privateKey}")
    private String privateKey;

    private RSA rsa;

    @PostConstruct
    public void init() {
        rsa = new RSA(privateKey, publicKey);
    }

    /**
     * 加密
     * @param dataStr 需要加密的数据
     * @return 加密数据
     */
    public String encrypt(String dataStr) {

        byte[] encrypt = rsa.encrypt(dataStr.getBytes(StandardCharsets.UTF_8), KeyType.PrivateKey);

        return Base64.getEncoder().encodeToString(encrypt);
    }

    /**
     * 解密
     * @param dataStr 加密数据
     * @return 解密数据
     */
    public String decode(String dataStr) {

        byte[] decrypt = rsa.decrypt(Base64.getDecoder().decode(dataStr.replaceAll(" ","+")), KeyType.PublicKey);

        return new String(decrypt);
    }

}
