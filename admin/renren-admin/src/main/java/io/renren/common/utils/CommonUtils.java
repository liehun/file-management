package io.renren.common.utils;/*
 * @Author WuYu
 * @Description
 * @Date 2023/09/07 11/28
 **/

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class CommonUtils {

    @Value("${file.downUrl}")
    public String uploadUrl;

    public static Integer getId(Integer id) {
        return id == null ? -1 : id;
    }
}
