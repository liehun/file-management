// api.js
//const baseApi = 'http://localhost:8080/renren-admin/'
const baseApi = 'http://121.37.240.237:9404/renren-admin/'

// 封装发送GET请求的函数
function get(url, data) {
  url = baseApi+url;
  uni.showLoading({
  	title:"网络请求中"
  })
  return new Promise((resolve, reject) => {
    uni.request({
      url: url,
      data: data,
	  header:{
		  'Token': getToken()
	  },
      method: 'GET',
      success: res => {
		uni.hideLoading()
        dealRes(res, resolve);
      },
      fail: err => {
		uni.hideLoading()
        reject(err);
      }
    });
  });
}

// 封装发送POST请求的函数
function post(url, data) {
  url = baseApi+url;
  uni.showLoading({
  	title:"网络请求中"
  })
  return new Promise((resolve, reject) => {
    uni.request({
      url: url,
      data: data,
	  Token: getToken(),
      method: 'POST',
      header: {
        'content-type': 'application/json',
		'Token': getToken()
      },
      success: res => {
		 uni.hideLoading()
		 dealRes(res, resolve)
      },
      fail: err => {
		uni.hideLoading()
        reject(err);
      }
    });
  });
}

function getToken(){
	return uni.getStorageSync("token");
}

function dealRes(res, resolve){
	let code = res.data.code;
	if(code == 0){
		resolve(res.data.data);
	} else {
		if(code == 401){
			//重新登录
			uni.reLaunch({
				url: "/pages/index/login"
			})
		} else {
			uni.showToast({
				title: res.data.msg,
				icon: 'none',
				duration: 2000
			});
		}
	}
}

export default {
  get,
  post
};